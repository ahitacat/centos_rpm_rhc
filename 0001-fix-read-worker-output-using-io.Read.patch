From d75dc60df73a88b0a14c799f3b6f1e8f66cee3d4 Mon Sep 17 00:00:00 2001
From: Link Dupont <link@sub-pop.net>
Date: Tue, 22 Nov 2022 13:07:41 -0500
Subject: [PATCH] fix: read worker output using io.Read

Some workers output a lot of text to stderr and stdout. Rather than
scanning stderr and stdout into a buffer using a bufio.Scanner, read a
fixed number of bytes at a time. This will break lines of output from
the worker in the middle of words, making reading stdout in the logs
more difficult, but avoids the overly verbose workers from hitting the
bufio.ErrTooLong error.

Signed-off-by: Link Dupont <link@sub-pop.net>
---
 cmd/yggd/exec.go | 46 +++++++++++++++++++++++++++++++++-------------
 1 file changed, 33 insertions(+), 13 deletions(-)

diff --git a/cmd/yggd/exec.go b/cmd/yggd/exec.go
index 4eb1757..a2a3d29 100644
--- a/cmd/yggd/exec.go
+++ b/cmd/yggd/exec.go
@@ -1,8 +1,8 @@
 package main
 
 import (
-	"bufio"
 	"fmt"
+	"io"
 	"io/ioutil"
 	"os"
 	"os/exec"
@@ -54,22 +54,42 @@ func startProcess(file string, env []string, delay time.Duration, died chan int)
 	log.Debugf("started process: %v", cmd.Process.Pid)
 
 	go func() {
-		scanner := bufio.NewScanner(stdout)
-		for scanner.Scan() {
-			log.Tracef("[%v] %v", file, scanner.Text())
-		}
-		if err := scanner.Err(); err != nil {
-			log.Errorf("cannot read from stdout: %v", err)
+		for {
+			buf := make([]byte, 4096)
+			n, err := stdout.Read(buf)
+			if n > 0 {
+				log.Tracef("[%v] %v", file, strings.TrimRight(string(buf), "\n\x00"))
+			}
+			if err != nil {
+				switch err {
+				case io.EOF:
+					log.Debugf("%v stdout reached EOF: %v", file, err)
+					return
+				default:
+					log.Errorf("cannot read from stdout: %v", err)
+					continue
+				}
+			}
 		}
 	}()
 
 	go func() {
-		scanner := bufio.NewScanner(stderr)
-		for scanner.Scan() {
-			log.Errorf("[%v] %v", file, scanner.Text())
-		}
-		if err := scanner.Err(); err != nil {
-			log.Errorf("cannot read from stderr: %v", err)
+		for {
+			buf := make([]byte, 4096)
+			n, err := stderr.Read(buf)
+			if n > 0 {
+				log.Errorf("[%v] %v", file, strings.TrimRight(string(buf), "\n\x00"))
+			}
+			if err != nil {
+				switch err {
+				case io.EOF:
+					log.Debugf("%v stderr reached EOF: %v", file, err)
+					return
+				default:
+					log.Errorf("cannot read from stderr: %v", err)
+					continue
+				}
+			}
 		}
 	}()
 
-- 
2.38.1

